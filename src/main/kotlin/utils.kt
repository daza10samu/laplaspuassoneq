import com.wolfram.jlink.KernelLink
import com.wolfram.jlink.MathLink
import com.wolfram.jlink.MathLinkFactory

object MathLinkObject {
    val ml: KernelLink = MathLinkFactory.createKernelLink("-linkmode launch -linkname 'math -mathlink'")

    init {
        ml.discardAnswer()
    }

    fun readMatrix(): MutableList<MutableList<Float>> {
        val result = mutableListOf<MutableList<Float>>()

        ml.function
        while (ml.ready()) {
            when (ml.type) {
                MathLink.MLTKFUNC -> when (ml.function.name) {
                    "List" -> result.add(mutableListOf())
                }

                MathLink.MLTKREAL -> result.last().add(ml.double.toFloat())

                else -> println(ml.type)
            }
        }

        return result
    }
}