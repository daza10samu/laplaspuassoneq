import org.jblas.DoubleMatrix
import org.jblas.Solve
import java.io.File
import java.io.FileWriter


class PuassonEq(
    val x0: Double,
    val xn: Double,
    val y0: Double,
    val yn: Double,
    val n: Int,
    val m: Int,
    val functionD: String,
    val functionG1: String,
    val functionG2: String,
    val functionG3: String,
    val functionG4: String,
) {
    val dX = (xn - x0) / n
    val x = List(n) { x0 + it * dX }
    val dY = (yn - y0) / m
    val y = List(m) { y0 + it * dY }
    lateinit var coefficientsMatrix: MutableList<MutableList<Double>>
    val ml = MathLinkObject.ml
    val e0 = 8.85418781762039e-12

    init {
        ml.evaluate("g1[x_]=$functionG1")
        ml.waitForAnswer()
        ml.newPacket()

        ml.evaluate("g2[x_]=$functionG2")
        ml.waitForAnswer()
        ml.newPacket()

        ml.evaluate("g3[y_]=$functionG3")
        ml.waitForAnswer()
        ml.newPacket()

        ml.evaluate("g4[y_]=$functionG4")
        ml.waitForAnswer()
        ml.newPacket()

        ml.evaluate("f[x_, y_]=$functionD")
        ml.waitForAnswer()
        ml.newPacket()
    }

    companion object {
        fun readFromFile(): PuassonEq {
            val lines = File("input.txt").readLines()
            return PuassonEq(
                lines[0].split(" ").map { it.toDouble() }[0],
                lines[0].split(" ").map { it.toDouble() }[1],
                lines[1].split(" ").map { it.toDouble() }[0],
                lines[1].split(" ").map { it.toDouble() }[1],
                lines[2].split(" ").map { it.toInt() }[0],
                lines[2].split(" ").map { it.toInt() }[1],
                lines[3],
                lines[4],
                lines[5],
                lines[6],
                lines[7]
            )
        }
    }

    fun solve() {
        var start = System.currentTimeMillis()

        val aCoeff = Array(n * m) { DoubleArray(n * m) }
        val bCoeff = DoubleArray(n * m)

        for (j in 0 until m) {
            aCoeff[j][j] = 1.0
            ml.evaluate("g3[${y0 + j * dY}]")
            ml.waitForAnswer()
            bCoeff[j] = ml.double

            aCoeff[m * n - j - 1][n * (m - 1) + j] = 1.0
            ml.evaluate("g4[${y0 + j * dY}]")
            ml.waitForAnswer()
            bCoeff[m * n - j - 1] = ml.double
        }

        for (i in 1 until n - 1) {
            aCoeff[i + m - 1][n * i] = 1.0
            ml.evaluate("g1[${x0 + i * dX}]")
            ml.waitForAnswer()
            bCoeff[i + m - 1] = ml.double

            aCoeff[i + m - 1 + n - 2][n * i + m - 1] = 1.0
            ml.evaluate("g2[${x0 + i * dX}]")
            ml.waitForAnswer()
            bCoeff[i + m - 1 + n - 2] = ml.double
        }

        for (i in 1 until n - 1) {
            for (j in 1 until m - 1) {
                aCoeff[m - 6 + 2 * n + i + (m - 3) * (i - 1) + j][(i + 1) * n + j] = 1 / (dX * dX)
                aCoeff[m - 6 + 2 * n + i + (m - 3) * (i - 1) + j][(i - 1) * n + j] = 1 / (dX * dX)
                aCoeff[m - 6 + 2 * n + i + (m - 3) * (i - 1) + j][i * n + j + 1] = 1 / (dY * dY)
                aCoeff[m - 6 + 2 * n + i + (m - 3) * (i - 1) + j][i * n + j - 1] = 1 / (dY * dY)
                aCoeff[m - 6 + 2 * n + i + (m - 3) * (i - 1) + j][i * n + j] = -2 * (1 / (dX * dX) + 1 / (dY * dY))

                ml.evaluate("f[${x0 + i * dX}, ${y0 + j * dY}]")
                ml.waitForAnswer()
                bCoeff[m - 6 + 2 * n + i + (m - 3) * (i - 1) + j] = -ml.double / e0
            }
        }

        val a = DoubleMatrix(aCoeff as Array<out DoubleArray>)
        val b = DoubleMatrix(bCoeff)

        println(System.currentTimeMillis() - start)
        println("\n\n\n")

        start = System.currentTimeMillis()
        val ans = Solve.solve(a, b)
        println(System.currentTimeMillis() - start)
        println("\n\n\n")

        val writer = FileWriter("res.dat")
        for(i in 0 until n){
            for(j in 0 until m){
                writer.write("${x0+i*(dX)} ${y0+j*dY} ${ans.get(i*m+j)}\n")
            }
        }
    }
}
