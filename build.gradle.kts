import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.10"
    application
}

group = "me.dazai"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven{
        url = uri("https://maven.jzy3d.org/releases/")
    }
}

dependencies {
    testImplementation(kotlin("test"))
    implementation(files("/home/dazai/.local/opt/Mathematica/SystemFiles/Links/JLink/JLink.jar"))
    implementation("org.jblas:jblas:1.2.5")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "16"
}

application {
    mainClass.set("MainKt")
}

//tasks.jar {
//    manifest {
//        attributes(
//            mapOf(
//                "Implementation-Title" to project.name,
//                "Implementation-Version" to project.version,
//                "Main-Class" to application.mainClass
//            )
//        )
//    }
//}

tasks.register<Jar>("uberJar") {
    archiveClassifier.set("uber")

    from(sourceSets.main.get().output)

    dependsOn(configurations.runtimeClasspath)
    from({
        configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) }
    })

    manifest {
        attributes(
            mapOf(
                "Implementation-Title" to project.name,
                "Implementation-Version" to project.version,
                "Main-Class" to application.mainClass
            )
        )
    }
}
